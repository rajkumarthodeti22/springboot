package com.ts.dao;




import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ts.model.Department;

@Service
public class DepartmentDAO {
	
	@Autowired
	DepartmentRepository depRepo;

	
	public List<Department> getAllDepatments() {
		return depRepo.findAll();
	}


	public Department getElementById(int id) {
		return depRepo.findById(id).orElse(new Department());
	}


	public Department registrationDepartment(Department emp) {
		return depRepo.save(emp);
	}


	public Department updateDepartment(Department emp) {
		return depRepo.save(emp);
	}


	public void deleteDepartmentById(int id) {
		depRepo.deleteById(id);
	}

}
package com.ts.EmpDept;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ts.dao.DepartmentDAO;
import com.ts.model.Department;


@RestController
public class DepartmentController {
	
	@Autowired
	DepartmentDAO Deptdao;
	
	@RequestMapping("showDepartments")
	public List<Department> getAllDepatments(){
		return Deptdao.getAllDepatments();
	}
	
	@RequestMapping("showElementById/{id}")
	public Department getElementById(@PathVariable int id){
		return Deptdao.getElementById(id);
	}
	
//	@RequestMapping("showDepartmentByName/{name}")
//	public Department getDepartmentByName(@PathVariable String name){
//		return dao.getDepartmentByName(name);
//	}
	
	@PostMapping("regEmp")
	public Department registerDepartment(@RequestBody Department emp){
		return Deptdao.registrationDepartment(emp);
	}
	
	@PutMapping("updateEmp")
	public Department updateDepartment(@RequestBody Department emp){
		return Deptdao.updateDepartment(emp);
	}
	
	@DeleteMapping("deleteEmp/{id}")
	public void deleteDepartmentById(@PathVariable int id){
		Deptdao.deleteDepartmentById(id);
	}
	
}

